<?php
//header("Content-Security-Policy: script-src 'self' 'nonce-123'");
header("Content-Security-Policy: script-src 'self' 'sha256-".base64_encode(hash('sha256', "alert('Hello');", true))."'");
?>
<!DOCTYPE html>
<html>
<head>
	<title>XSS - Sample 4</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
	<script type="text/javascript" charset="utf-8">alert('Hello');</script>
</head>
<body>
	<div class="container">
		<h1 class="page-header">XSS Honeypot</h1>
	</div>
</body>
</html>