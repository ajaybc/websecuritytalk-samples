<?php
header("X-XSS-Protection: 0");
?>
<!DOCTYPE html>
<html>
<head>
	<title>XSS - Sample 1</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1 class="page-header">XSS Honeypot</h1>
		<p>
			<?php
			if ($_POST['xss']) {
				echo $_POST['xss'];
			} else {
			?>
				Type something in the text box below and I will show it here.
			<?php
			}
			?>
		</p>
		<form method="POST">
			<div class="form-group">
				<textarea name="xss" class="form-control" placeholder="Enter text"><?php echo $_POST['xss'];?></textarea>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="Submit" />
			</div>
		</form>
	</div>
</body>
</html>