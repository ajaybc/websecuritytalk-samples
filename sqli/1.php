<?php
//header("X-XSS-Protection: 0");
?>
<!DOCTYPE html>
<html>
<head>
	<title>SQLi - Sample 1</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1 class="page-header">Dumb Login Server</h1>
		<p>
			<?php
			if ($_POST) {
				echo 'SELECT * FROM users WHERE email="'.$_POST['email'].'" AND password="'.$_POST['password'].'"';
			} else {
			?>
				Please login with whatever username or password you like
			<?php
			}
			?>
		</p>
		<form method="POST">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<input type="email" name="email" placeholder="Enter Email" required="required" class="form-control"/>
					</div>
					<div class="form-group">
						<input type="text" name="password" placeholder="Enter Password" required="required" class="form-control"/>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Submit" />
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>